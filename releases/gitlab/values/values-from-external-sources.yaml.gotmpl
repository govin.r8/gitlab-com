---
{{- $env_prefix := .Environment.Values | getOrNil "env_prefix" | default .Environment.Name }}
{{- $chef_source_name := .Environment.Values | getOrNil "gitlab_chef_source_name" | default $env_prefix }}
{{- $get_role_from_chef := "../../../bin/get-role-from-chef" }}
{{- $chef_base_role := exec $get_role_from_chef (list (print $chef_source_name "-base")) }}
{{- $chef_base_be_role := exec $get_role_from_chef (list (print $chef_source_name "-base-be")) }}
{{- $chef_base_be_sidekiq_role := exec $get_role_from_chef (list (print $chef_source_name "-base-be-sidekiq")) }}
{{- $chef_base_fe_role := exec $get_role_from_chef (list (print $chef_source_name "-base-fe")) }}

{{- /* As the pre environment has no redis specific roles, we only fetch for other environments */ -}}
{{- $chef_base_db_redis_server_sidekiq_role := "{}" }}
{{- $chef_base_db_redis_server_cache_role := "{}" }}
{{- if ne "pre" .Environment.Name }}
{{- $chef_base_db_redis_server_sidekiq_role = exec $get_role_from_chef (list (print $chef_source_name "-base-db-redis-server-sidekiq")) }}
{{- $chef_base_db_redis_server_cache_role = exec $get_role_from_chef (list (print $chef_source_name "-base-db-redis-sentinel-cache")) }}
{{- end }}

{{- $gkms_source := .Environment.Values | getOrNil "gitlab_secrets_gkms_source" | default $env_prefix }}
{{- $gkms_omnibus_secrets := exec "bash" (list "-c" (print "gsutil cat gs://gitlab-" $gkms_source "-secrets/gitlab-omnibus-secrets/" $gkms_source ".enc | gcloud --project " .Environment.Values.google_project " kms decrypt --location global --keyring=gitlab-secrets --key " $gkms_source " --ciphertext-file=- --plaintext-file=-")) }}
global:
  gitaly:
    external:
{{ $chef_base_role | exec "jq" (list "-rf" "jq/global-gitaly-external.jq") | indent 6 }}

  email:
    from: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".gitlab_email_from") }}

  appConfig:
    incomingEmail:
      address: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".incoming_email_address") }}
      user: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".incoming_email_email") }}

    serviceDeskEmail:
      enabled: {{ $chef_base_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".service_desk_email_enabled") }}
      address: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".service_desk_email_address") }}
      user: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".service_desk_email_email") }}

    lfs:
      enabled: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".lfs_object_store_enabled") }}
      bucket: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".lfs_object_store_remote_directory") }}
    artifacts:
      enabled: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".artifacts_object_store_enabled") }}
      bucket: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".artifacts_object_store_remote_directory") }}
    uploads:
      enabled: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".uploads_object_store_enabled") }}
      bucket: {{ $chef_base_role | exec "jq" ( list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".uploads_object_store_remote_directory") }}
    packages:
      enabled: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".packages_object_store_enabled") }}
      bucket: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".packages_object_store_remote_directory") }}
    externalDiffs:
      enabled: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".external_diffs_object_store_enabled") }}
      bucket: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".external_diffs_object_store_remote_directory") }}
    terraformState:
      enabled: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".terraform_state_object_store_enabled") }}
      bucket: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".terraform_state_object_store_remote_directory") }}
    dependencyProxy:
      enabled: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".dependency_proxy_object_store_enabled") }}
      bucket: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".dependency_proxy_object_store_remote_directory") }}

    omniauth:
      enabled: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".omniauth_enabled") }}
      blockAutoCreatedUsers: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".omniauth_block_auto_created_users") }}
    cron_jobs:
      {{- $pipeline_schedule_worker_cron := $chef_base_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".pipeline_schedule_worker_cron") }}
      {{- $schedule_migrate_external_diffs_worker_cron := $chef_base_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".schedule_migrate_external_diffs_worker_cron") }}
      {{- if ne "null" $pipeline_schedule_worker_cron }}
      pipeline_schedule_worker:
        cron: '{{ $pipeline_schedule_worker_cron }}'
      {{- end }}
      {{- if ne "null" $schedule_migrate_external_diffs_worker_cron }}
      schedule_migrate_external_diffs_worker:
        cron: '{{ $schedule_migrate_external_diffs_worker_cron }}'
      {{- end }}
    sentry:
      enabled: {{ $chef_base_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".sentry_enabled") }}
      dsn: {{ $chef_base_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".sentry_dsn") }}
      clientside_dsn: {{ $chef_base_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".sentry_clientside_dsn") }}
      environment: {{ $chef_base_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".sentry_environment") }}
  hosts:
    externalIP: {{ exec "gcloud" (list "--project" .Environment.Values.google_project "compute" "addresses" "list" "--filter" (printf "name=nginx-gke-%s" .Environment.Name) "--format" "value(address)" "--verbosity" "error") }}
    registry:
      name: {{ regexReplaceAll "https://" ($chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.registry_external_url")) "" }}

  pages:
    host: {{ $chef_base_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".gitlab_rb.pages_external_url | ltrimstr(\"https://\")") }}

  psql:
    host: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".db_host") }}
    port: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".db_port") }}
    username: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".db_username") }}
  smtp:
    address: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".smtp_address") }}
    authentication: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".smtp_authentication") }}
    domain: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".smtp_domain") }}
    starttls_auto: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".smtp_enable_starttls_auto") }}
    user_name: {{ $gkms_omnibus_secrets | exec "jq" (list ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".smtp_user_name") }}

  redis:
  {{- $redis_queues_cluster_name := $chef_base_db_redis_server_sidekiq_role | exec "jq" (list "-j" ".override_attributes.\"omnibus-gitlab\".gitlab_rb.redis.master_name") }}
  {{- $redis_cache_cluster_name := $chef_base_db_redis_server_cache_role | exec "jq" (list "-j" ".override_attributes.\"omnibus-gitlab\".gitlab_rb.redis.master_name") }}
  {{- $redis_cluster_name := $chef_base_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".gitlab_rb.redis.master_name") }}
  {{- $redis_queues_sentinels := $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".redis_queues_sentinels") }}
  {{- $redis_cache_sentinels := $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".redis_cache_sentinels") }}
  {{- $redis_sentinels := $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".redis_sentinels") }}

  {{- if eq "null" $redis_queues_cluster_name }}
    host: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".redis_host") }}
    port: {{ $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".redis_port") }}
    password:
      enabled: {{ ne "null" ($gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".redis_password")) }}
  {{- else }}
    host: {{ $redis_cluster_name }}
    sentinels: {{ $redis_sentinels }}
    cache:
      host: {{ $redis_cache_cluster_name }}
      sentinels: {{ $redis_cache_sentinels }}
      password:
        enabled: true
        secret: gitlab-redis-credential-v1
        key: secret
    sharedState:
      host: {{ $redis_cluster_name }}
      sentinels: {{ $redis_sentinels }}
      password:
        enabled: true
        secret: gitlab-redis-credential-v1
        key: secret
    queues:
      host: {{ $redis_queues_cluster_name }}
      sentinels: {{ $redis_queues_sentinels }}
      password:
        enabled: true
        secret: gitlab-redis-credential-v1
        key: secret
  {{- end }}

registry:
  service:
    loadBalancerIP: {{ exec "gcloud" (list "--project" .Environment.Values.google_project "compute" "addresses" "list" "--filter" (printf "name=registry-gke-%s" .Environment.Name) "--format" "value(address)" "--verbosity" "error") }}

{{- $loadBalancerIPShell := exec "gcloud" (list "--project" .Environment.Values.google_project "compute" "addresses" "list" "--filter" (printf "name=ssh-gke-%s" .Environment.Name) "--format" "value(address)" "--verbosity" "error") }}
gitlab:
  kas:
    service:
      loadBalancerIP: {{ exec "gcloud" (list "--project" .Environment.Values.google_project "compute" "addresses" "list" "--filter" (printf "name=kas-internal-gke-%s" .Environment.Name) "--format" "value(address)" "--verbosity" "error") }}
  gitlab-shell:
    service:
      type: LoadBalancer
{{- if $loadBalancerIPShell }}
      loadBalancerIP: {{ $loadBalancerIPShell }}
{{- end }}
      annotations:
        cloud.google.com/load-balancer-type: Internal

{{- $workhorse_dsn := $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-gitlab-workhorse\".env.GITLAB_WORKHORSE_SENTRY_DSN") }}
{{- $rack_attack_enabled := $chef_base_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".rack_attack_git_basic_auth.enabled") }}
{{- $rack_attack_ip_whitelist := $chef_base_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".rack_attack_git_basic_auth.ip_whitelist") }}
  webservice:
    deployments:
      git:
        service:
          loadBalancerIP: {{ exec "gcloud" (list "--project" .Environment.Values.google_project "compute" "addresses" "list" "--filter" (printf "name=git-https-gke-%s" .Environment.Name) "--format" "value(address)" "--verbosity" "error") }}
      websockets:
        service:
          loadBalancerIP: {{ exec "gcloud" (list "--project" .Environment.Values.google_project "compute" "addresses" "list" "--filter" (printf "name=websockets-gke-%s" .Environment.Name) "--format" "value(address)" "--verbosity" "error") }}
    extraEnv:
      GITLAB_THROTTLE_USER_ALLOW_LIST: {{ $chef_base_fe_role | exec "jq" (list "-j" ".default_attributes.\"omnibus-gitlab\".user_ratelimit_bypasses | keys? | join(\",\")") | quote }}
      GITLAB_UPLOAD_API_ALLOWLIST: {{ $gkms_omnibus_secrets | exec "jq" (list "-j" ".\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".env.GITLAB_UPLOAD_API_ALLOWLIST | select(.!=null)") | quote }}
    rack_attack:
      git_basic_auth:
        enabled: {{ $rack_attack_enabled }}
        ip_whitelist: {{ $rack_attack_ip_whitelist }}
        maxretry: 30
        findtime: 180
        bantime: 3600
    workhorse:
      sentryDSN: {{ $workhorse_dsn }}
  sidekiq:
    psql:
      database: {{ $chef_base_be_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".db_database") }}
      host: {{ $chef_base_be_sidekiq_role | exec "jq" (list ".default_attributes.\"omnibus-gitlab\".gitlab_rb.\"gitlab-rails\".db_host") }}
