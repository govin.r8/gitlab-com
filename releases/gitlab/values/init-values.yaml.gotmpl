{{/*

COMPONENT - determines what components we'd like to deploy.
  * This defaults to the gitlab application which encompasses all GitLab services in the GitLab sub-chart https://gitlab.com/gitlab-org/charts/gitlab/-/tree/master/charts/gitlab
  * acceptable values are currently limited to:
    * `gitlab-registry`
    * `gitlab`

COMPONENT_VERSION - determines the version of the component to be deployed
  * For backwards compatability, we set this from `GITLAB_IMAGE_TAG` as well
  * Removal of backwards compatability is tracked in issue: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1222

It is possible to set a new version by setting $COMPONENT_VERSION
in the environment.

This is being done now for normal deploys until deployments switch to using a helm chart release.
This is being tracked in issue https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/697

*/}}

{{/* Initialize */}}
{{- $namespace := .Environment.Values | getOrNil "gitlab_namespace" | default "gitlab" }}
{{- $gitlabChartInfo := printf "%s%s" $namespace "-gitlab-chart-info" }}
{{- $provided_image_tag := default (env "GITLAB_IMAGE_TAG") (env "COMPONENT_VERSION") }}

{{/* Get current version of the GitLab Application */}}
{{- $gitlab_current_spec := exec "kubectl" (list "get" "configmap" $gitlabChartInfo "--namespace" $namespace "--output" "jsonpath={.data.gitlabVersion}" "--ignore-not-found") }}
{{- $gitlab_current_tag := ($gitlab_current_spec | exec "cut"  (list "-d:" "-f3")) }}
{{- $gitlab_version := $gitlab_current_tag }}

{{/* Get current version of the Container Registry */}}
{{- $registry_deployment_name := .Environment.Values | getOrNil "registry_deployment_for_image_tag" }}
{{- $registry_current_spec := exec "kubectl" (list "get" "deployment" $registry_deployment_name "--namespace" $namespace "--output" "jsonpath={.spec.template.spec.containers[0].image}" "--ignore-not-found") }}
{{- $registry_current_tag := ($registry_current_spec | exec "cut"  (list "-d:" "-f3")) }}
{{- $registry_version := $registry_current_tag }}

{{/* Get the Deployment we are attempting to modify */}}
{{- $component := default "gitlab" (env "COMPONENT") }}

{{/* Set the version of the Deployment that we'd like to change */}}
{{- if eq $component "gitlab-registry" }}
{{-   $registry_version = default $registry_version $provided_image_tag }}
{{- else if eq $component "gitlab" }}
{{-   $gitlab_version = default $gitlab_version $provided_image_tag }}

{{/*

After the warning noted below can be removed,
we should be able to remove this fall back as well

Issue to track: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1222

*/}}

{{- else }}
{{-   $gitlab_version = default $gitlab_version $provided_image_tag }}
{{- end }}

{{/*

:warning:

We are currently in a state of flux.  We've not fully implemented
the capability to trigger deploys for the Container Registry, but
helmfile assumes we can.  Until a deployment pipeline can be
triggered with the desired version, let's hardcode the desired version
here until we are ready.

Issue to track: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1222

:warning:

*/}}

{{- $registry_version = "v3.5.0-gitlab" }}

{{/*

GitLab-Shell contains it's own versioning schema.  This prevents us from
using the same $gitlab_version variable for this component.  Instead
we determine if this is an auto-deploy.  If yes, use that tag, if not
default to the version specified by the whatever is currently running.

*/}}

{{- $gitlab_shell_deployment_name := printf "%s%s" $namespace "-gitlab-shell" }}

{{/* Get current version of the GitLab Shell */}}
{{- $gitlab_shell_current_spec := exec "kubectl" (list "get" "deployment" $gitlab_shell_deployment_name "--namespace" $namespace "--output" "jsonpath={.spec.template.spec.containers[0].image}" "--ignore-not-found") }}
{{- $gitlab_shell_current_tag := ($gitlab_shell_current_spec | exec "cut"  (list "-d:" "-f3")) }}

{{/*
Set GitLab-Shell Version:
  * If auto-deploy utilize the version provided by $gitlab_version
  * If an RC, use special version `"gitlab-%s", $gitlab_version`
  * Otherwise fall back to what is currently running, $gitlab_shell_current_tag
  * Otherwise `master` if no GitLab-Shell is running
The RC specific logic should be removed during https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1553
*/}}
{{- $gitlab_shell_version := default "master" $gitlab_shell_current_tag }}
{{- if regexMatch "^\\d+-\\d+-[\\d\\w]+-[\\d\\w]+$" $gitlab_version }}
{{-   $gitlab_shell_version = $gitlab_version }}
{{- else if regexMatch "^v\\d+\\.\\d+\\.\\d+-rc\\d+$" $gitlab_version }}
{{-   $gitlab_shell_version = printf "gitlab-%s" $gitlab_version }}
{{- end }}

---

gitlab:
  gitlab-shell:
    image:
      tag: {{ $gitlab_shell_version }}
{{/* To be changed to something more dynamic, tracked in https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1544 */}}
  kas:
    image:
      tag: v13.12.1
global:
  gitlabVersion: {{ $gitlab_version }}
registry:
  image:
    tag: {{ $registry_version }}
