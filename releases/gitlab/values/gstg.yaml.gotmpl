---

registry:
  image:
    tag: v3.5.0-gitlab
  nodeSelector:
    type: registry

gitlab:
  webservice:
    minReplicas: 2
    maxReplicas: 30
    extraEnv:
      ENABLE_ACTIVERECORD_TYPEMAP_CACHE: "true"
      ENABLE_RAILS_61_CONNECTION_HANDLING: "true"
      GITLAB_THROTTLE_BYPASS_HEADER: "X-GitLab-RateLimit-Bypass"
      GITLAB_THROTTLE_DRY_RUN: "throttle_unauthenticated,throttle_authenticated_api,throttle_authenticated_web"
      DISABLE_PUMA_NAKAYOSHI_FORK: "true"
      GITLAB_SIDEKIQ_SIZE_LIMITER_MODE: track
      GITLAB_SIDEKIQ_SIZE_LIMITER_LIMIT_BYTES: 100000
      GITLAB_CDN_HOST: https://gl-staging.freetls.fastly.net
      USE_NEW_LOAD_BALANCER_QUERY: "true"
    workhorse:
      resources:
        limits:
          memory: 2G
        requests:
          cpu: 600m
          memory: 200M
    resources:
      limits:
        memory: 6.0G
      requests:
        cpu: 4
        memory: 5G
  kas:
    workhorse:
      scheme: 'https'
      host: 'int.gstg.gitlab.net'
      port: 11443
    customConfig:
      observability:
        sentry:
          dsn: https://af4940ad841b46d78708595fc654af78@sentry.gitlab.net/124
          environment: {{ .Environment.Values | getOrNil "env_prefix" }}
  sidekiq:
    extraEnv:
      SIDEKIQ_SEMI_RELIABLE_FETCH_TIMEOUT: 5
      ENABLE_LOAD_BALANCING_FOR_SIDEKIQ: "true"
      ENABLE_ACTIVERECORD_TYPEMAP_CACHE: "true"
      ENABLE_RAILS_61_CONNECTION_HANDLING: "true"
      USE_GITLAB_LOGGER: 1
      GITLAB_SIDEKIQ_SIZE_LIMITER_MODE: track
      GITLAB_SIDEKIQ_SIZE_LIMITER_LIMIT_BYTES: 100000
    extraInitContainers: |
      - name: write-instance-name
        args:
          - -c
          - echo "$INSTANCE_NAME" > /etc/gitlab/instance_name
        command:
          - sh
        env:
          - name: INSTANCE_NAME
            valueFrom:
              fieldRef:
                fieldPath: spec.nodeName
        image: 'busybox:latest'
        volumeMounts:
          - mountPath: /etc/gitlab
            name: sidekiq-secrets
    psql:
      load_balancing:
        discover:
          nameserver: <%= File.read('/etc/gitlab/instance_name').strip %>
          record: db-replica.service.consul.
          record_type: SRV
          port: 8600
          use_tcp: true

global:
  appConfig:
    incomingEmail:
      enabled: true
    omniauth:
      providers:
        - secret: gitlab-google-oauth2-v1
        - secret: gitlab-twitter-oauth2-v1
        - secret: gitlab-github-oauth2-v1
        - secret: gitlab-bitbucket-oauth2-v1
        - secret: gitlab-group-saml-oauth2-v1
        - secret: gitlab-salesforce-oauth2-v1
    sidekiq:
      routingRules:
        - ["name=project_import_schedule", null] # we cannot migrate this worker yet, https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1064
        - ["resource_boundary=cpu&urgency=high&tags!=requires_disk_io", null] # urgent-cpu-bound
        - ["resource_boundary=memory", null] # memory-bound
        - ["feature_category=global_search&urgency=throttled", null] # elasticsearch
        - ["resource_boundary!=cpu&urgency=high", null] # urgent-other
        - ["resource_boundary=cpu&urgency=default,low", null] # low-urgency-cpu-bound
        - ["feature_category=database&urgency=throttled", null] # database-throttled
        - ["feature_category=gitaly&urgency=throttled", null] # gitaly-throttled
        - ["tags=exclude_from_kubernetes", null] # catchall on VMs
        - ["tags=exclude_from_gitlab_com", "default"] # special case for testing (only runs actively on staging; includes geo jobs)
        - ["*", null] # catchall on k8s

  email:
    reply_to: noreply@gitlab.com

  geo:
    enabled: true
    nodeName: {{ .Environment.Values | getOrNil "gitlab_domain" | default "" }}
    role: primary
    registry:
      syncSecret:
        secret: gitlab-registry-notification-v1
    replication:
      enabled: true

  hosts:
    gitlab:
      name: staging.gitlab.com
    kas:
      name: kas.staging.gitlab.com

  psql:
    prepared_statements: false
    load_balancing:
      discover:
        nameserver: <%= File.read('/etc/gitlab/instance_name').strip %>
        record: db-replica.service.consul.
        record_type: SRV
        port: 8600
        use_tcp: true

  smtp:
    enabled: true
    password:
      key: secret
      secret: gitlab-smtp-credential-v1
